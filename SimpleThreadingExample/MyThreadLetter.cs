﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadingExample
{
    class MyThreadLetter
    {

        private ManualResetEvent SignalExit;
        private AutoResetEvent SignalVoyelle;

        public Thread LaTache;

        public MyThreadLetter(ManualResetEvent SignalExit, AutoResetEvent SignalVoyelle)
        {
            // Création d'un thread, sans le démarrer
            LaTache = new Thread(new ThreadStart(RunLetter));
            this.SignalExit = SignalExit;
            this.SignalVoyelle = SignalVoyelle;
        }


        public void Start()
        {
            try
            {
                LaTache.Start();
                Console.WriteLine("Letter démarré correctement...");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Letter a déjà été démarré...");
            }
        }

        private void RunLetter()
        {
            // Un random pour le sleep...
            Random rnd = new Random();
            // La lettre à afficher
            char letter = 'a';

            while (!SignalExit.WaitOne(rnd.Next(100, 1001), false))
            {
                /*
                if (letter < 'z')
                    letter++;
                else
                    letter = 'a';
                Console.WriteLine("\t\t---> Letter: " + letter.ToString());
                // Dodo pour un temps aléatoire < 1267ms
                //Thread.Sleep(rnd.Next(1267));*/

                letter = (char)rnd.Next(97, 123);
                Console.WriteLine("\t\t-----> Letter: " + letter.ToString()); 
                switch (letter)
                {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                    case 'y':
                        SignalVoyelle.Set();
                        break;
                    default:
                        break;
                }
            }

            Console.WriteLine("\t\t---> Le threadLetter a fini son travail.");
        }

        

    }
}
