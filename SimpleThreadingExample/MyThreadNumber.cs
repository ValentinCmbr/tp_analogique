﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadingExample
{
    class MyThreadNumber
    {
        private object verrou = new object();
        private int compteurVoyelle;

        private ManualResetEvent SignalExit;
        private AutoResetEvent SignalVoyelle;

        public Thread LaTache;

        public MyThreadNumber(ManualResetEvent SignalExit, AutoResetEvent SignalVoyelle)
        {
            // Création d'un thread, sans le démarrer
            LaTache = new Thread(new ThreadStart(RunCompteurVoyelle));
            this.SignalExit = SignalExit;
            this.SignalVoyelle = SignalVoyelle;
        }

        public void Start(int max = 999)
        {
            try
            {
                LaTache.Start();
                Console.WriteLine("Number démarré correctement...");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Number a déjà été demarré...");
                throw ex;
            }
            catch (OutOfMemoryException ex)
            {
                Console.WriteLine("Number ne peut pas démarrer par manque de mémoire...");
                throw ex;
            }
        }

        private void RunNumber(object max)
        {
            Random rnd = new Random();
            int Number = 0;

            while (!SignalExit.WaitOne(rnd.Next(100, 1001), false))
            {
                if (Number < (int)max)
                    Number++;
                else
                    Number = 0;

                Console.WriteLine("=======> Number: " + Number.ToString());

            }
            Console.WriteLine("Le threadNumber a fini son travail.");
        }



        private void RunCompteurVoyelle()
        {
            // Nos signaux signalXXX sont des WaitHandle. On les range dans un tableau.
            WaitHandle[] LesSignaux = { SignalExit, SignalVoyelle };
            int indexSignal = -1; // On aura besoin d'un index pour ce tableau
            bool fin = false; // Passera à true lorsqu'il faudra s'arréter

            // Attend que fin passe à true
            while (!fin)
            {
                // Attente bloquante d'un des signaux du tableau LesSignaux
                // indexSignal contiendra l'index dans le tableau de celui déclenché
                indexSignal = WaitHandle.WaitAny(LesSignaux);
                switch (indexSignal)
                {
                    // C'est le SignalVoyelle qui est activé
                    case 1:
                        lock (verrou)
                        {
                            compteurVoyelle++;
                        }
                        break;
                    // C'est le SignalExit qui est activé
                    case 0:
                        fin = true;
                        break;
                }
                Console.WriteLine("=======> Number: " + compteurVoyelle.ToString() + " Voyelle(s)");

            }
            Console.WriteLine("Le threadNumber a fini son travail.");

        }

    }
}

