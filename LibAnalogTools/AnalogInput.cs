﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using MccDaq;

namespace LibAnalogTools
{
    public class AnalogInput
    {
        private int channel;
        private ushort mesureBrute;
        private float mesureVolt;
        private MccBoard acqBoard;

        public int Channel { get => channel; }

        public ushort MesureBrute
        {
            get
            {
                var mesureBrute = new UInt16();
                acqBoard.AIn(this.channel, Range.Bip10Volts, out mesureBrute);
                return mesureBrute;
            }
        }

        public float MesureVolt
        {
            get
            {
                var mesureVolt = new float();
                acqBoard.VIn(this.channel, Range.Bip10Volts, out mesureVolt, 0);
                return mesureVolt;
            }
        }

        public AnalogInput(MccBoard board, int channel = 0)
        {
            this.acqBoard = board;
            this.channel = channel;
        }

    }
}

