﻿using MccDaq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace LibAnalogTools
{
    public class AnalogInputThread
    {
        private ushort mesureBrute;
        private float mesureVolt;

        public ushort MesureBrut { get; }
        public ushort MesureVolt { get; }
        public int Channel { get; }
        public int PeriodeEchantillonage { get; set; }
        public string Nom { get; }
        public Thread LaTache { get; }
        public MccBoard AcqBoard { get; }
        public AutoResetEvent SignalExit { get; }
        public AnalogInputThread(MccBoard board, int channel = 0, int periodeEchMs = 0, string nom = "")
        {
            LaTache = new Thread(new ThreadStart(runMesure));
            this.AcqBoard = board;
            this.Channel = channel;
            this.PeriodeEchantillonage = periodeEchMs;
            this.Nom = nom;
        }
        private void runMesure()
        {
            while (!SignalExit.WaitOne(PeriodeEchantillonage, false))
            {
                AcqBoard.AIn(Channel, Range.Bip10Volts, out mesureBrute);
                AcqBoard.VIn(Channel, Range.Bip10Volts, out mesureVolt, VInOptions.Default);
                Thread.Sleep(1000);
            }
        }
        public void Start()
        {
            try
            {
                LaTache.Start();
                Console.WriteLine("Number démarré");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Number a déjà démarré ");
            }
            catch (OutOfMemoryException ex)
            {
                Console.WriteLine("Number ne peut pas démarrer à cause d'un manque de mémoire");
            }
        }
        public void Stop()
        {
            SignalExit.Set();
        }
        public void Join()
        {
            LaTache.Join();
        }
    }
}
